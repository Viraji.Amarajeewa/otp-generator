package com.example.otpGenerator;

import java.util.Random;

public class UserData {

    private int phoneNum;
    private int otp;


    public int getPhoneNum() {
        return phoneNum;
    }


    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }

    public int getOTP() {
        return otp;
    }
}