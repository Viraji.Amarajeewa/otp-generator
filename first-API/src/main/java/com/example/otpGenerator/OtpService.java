package com.example.otpGenerator;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static com.example.otpGenerator.Controller.data_list;

@org.springframework.stereotype.Service
public class OtpService {

   // verify OTP
    public boolean verifyOTP(int number, int otp) {
        for(int i = 0; i < data_list.size(); i++) {
            if(data_list.get(i).get(0) == number && data_list.get(i).get(1) == otp ){
                    return  true;
                }else {
                return false;
            }

        }
        return false;
    }

    //create an return random OTP
    public int createOTP() {
        int num = (int) (Math.random()*90000 + 100000);
        return num;
    }

}
