package com.example.otpGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class Controller {

    @Autowired
    private OtpService otpservice;
    public static ArrayList<ArrayList<Integer>> data_list = new ArrayList<ArrayList<Integer>>() ;
    int count = 0;

    @PostMapping("/user/verify")
    public boolean verifyOTP(@RequestBody UserData userData) {
        return otpservice.verifyOTP(userData.getPhoneNum(), userData.getOTP());
    }

    @PostMapping("/user")
    public ResponseEntity<?> createUser(@RequestBody UserData userData) {
        int otp = otpservice.createOTP();
        System.out.println("OTP :"+otp);
        data_list.add(count, new ArrayList<>(Arrays.asList(userData.getPhoneNum(), otp)) );
//        System.out.println(data_list);
        count++;
        return ResponseEntity.ok().body(HttpStatus.OK);
    }
}
